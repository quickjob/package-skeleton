<?php namespace Quickjob\Skeleton;

use Mockery;
define(__ROOT__,realpath(__DIR__.'/../'));

function mkdir($path,$chmod,$recursive) {
	CreatePackageTest::makeDirectory($path);
}

function file_put_contents($file,$contents) {
	CreatePackageTest::editFile($file);
}

function unlink($file) {
	CreatePackageTest::removedFiles($file);
}

function readline() {
	return '';
}

class CreatePackageTest extends \PHPUnit_Framework_TestCase
{
	static $removed = array();
	static $mkdir = array();
	static $edited = array();
	static function removedFiles($file)
	{
		self::$removed[] = $file;
	}
	
	static function makeDirectory($file)
	{
		self::$mkdir[] = $file;
	}

	static function editFile($file)
	{
		self::$edited[] = $file;
	}

	public function tearDown()
	{
		Mockery::close();
	}
	public function setUp()
	{
		parent::setUp();
		$this->pkg = Mockery::mock('Quickjob\Package');
		$this->pkg->name = 'auth';
		$this->pkg->description = 'Simple auth service';
		$this->author = Mockery::mock('Quickjob\Author');
		$this->author->shouldReceive('getName')->andReturn('John Doe');
		$this->author->shouldReceive('getEmail')->andReturn('john@example.com');

		$this->creator = new CreatePackage($this->pkg,$this->author);
		$this->composer = $this->creator->fixComposer();

	}
	public function test_can_get_author()
	{
		$a = new Author();
		$this->assertNotNull($a->getName(), 'Can get author name!');
	}
	public function test_if_empty_packagename_returns_current_basename()
	{
		$p = new Package();
		$this->assertEquals($p->name, 'package-skeleton');
	}

	public function test_if_directory_has_the_correct_name()
	{
		$this->assertEquals($this->creator->getSrcPath(), __ROOT__ . '/src');
		$this->assertEquals($this->creator->getServiceProviderClass(), 'AuthServiceProvider');
	}

	public function test_if_composer_was_fixed()
	{
		$this->composerShouldHave("quickjob/auth");
		$this->composerShouldHave($this->pkg->description);
		$this->composerShouldHave("John Doe");
		$this->composerShouldHave("john@example.com");
		$this->composerShouldHave("Quickjob\\\\Auth");
		$this->composerShouldNotHave("src/create-files.php");
		$this->composerShouldNotHave("php create-files.php");
	}

	public function composerShouldHave($value)
	{
		$specDescription = "composer.json should have: ".$value."\n".$this->composer;
		$search = strpos($this->composer,$value);
		$this->assertNotEquals($search,false,$specDecription);
	}
	public function composerShouldNotHave($value)
	{
		$specDescription = "composer.json should have: ".$value."\n".$this->composer;
		$search = strpos($this->composer,$value);
		$this->assertEquals($search,false,$specDecription);
	}

	public function test_if_files_are_correctly_deleted() {
		$this->creator->setupPackage();
		$toBeRemoved = [
			__ROOT__ . '/tests/CreatePackageTest.php',
			__ROOT__ . '/composer.json.tpl',
			__ROOT__ . '/ServiceProviderTemplate.txt',
			__ROOT__ . '/create-files.php',
		];
		$toBeEdited = [
			__ROOT__ . '/phpunit.xml',
			__ROOT__ . '/composer.json',
			__ROOT__ . '/README.md',
			__ROOT__ . '/src/AuthServiceProvider.php',
		];

		$this->assertEquals(self::$removed, $toBeRemoved);
		$this->assertEquals(self::$edited, $toBeEdited);
		// $this->assertEquals([__ROOT__ . '/src'],self::$mkdir);
	}
}
