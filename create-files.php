<?php namespace Quickjob\Skeleton;

if(!function_exists('readline')) {
	function readline($msg) {
  		return null;
	}
}

class Package {
	public $name;
	public $description;

	function __construct() {
		$this->name = basename(__DIR__);
		$name = readline("Please inform the package name (empty for {$this->name}): ");
		if(!empty($name)){
			$this->name = $name;
		}
			
		$this->description = readline("Please inform the package description: ");
	}
}

class Author {
	public function getName()
	{
		$name = shell_exec('git config user.name') ?: 'author';
		return trim($name,"\n");
	}

	public function getEmail()
	{
		$email = shell_exec('git config user.email') ?: 'author@email.com';
		return trim($email,"\n");
	}
}

class CreatePackage {
	protected $author;
	protected $package;
	protected $className;
	
	function __construct($package,$author) {
		$this->author = $author;
		$this->package = $package;
		$this->className = ucfirst($this->package->name);
	}

	public function fixComposer()
	{
		$composer = file_get_contents(__DIR__ . '/composer.json.tpl');
		$composer = $this->doReplacements($composer);

		$json = json_decode($composer,true);
		unset($json['autoload']['files']);
		unset($json['scripts']);
		$composer = json_encode($json,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
		return $composer;
	}

	protected function doReplacements($string)
	{
		$string = str_replace('{{packagename}}', strtolower($this->package->name), $string);
		$string = str_replace('{{description}}', $this->package->description, $string);
		$string = str_replace('{{author}}', $this->author->getName(), $string);
		$string = str_replace('example@example.com', $this->author->getEmail(), $string);
		$string = str_replace(array('Skeleton','{{classname}}'), $this->className, $string);
		$string = str_replace('{{serviceprovider}}', $this->getServiceProviderClass(), $string);
		return $string;
	}

	public function getSrcPath()
	{
		return __DIR__ . "/src";
	}

	public function getServiceProviderClass() {
		return $this->className."ServiceProvider";
	}

	public function getServiceProviderFile() {
		$f = file_get_contents(__DIR__ . "/ServiceProviderTemplate.txt");
		$f = $this->doReplacements($f);
		return $f;
	}

	public function getRoutesFile() {
		$f = file_get_contents(__DIR__ . "/routesTemplate.txt");
		$f = $this->doReplacements($f);
		return $f;
	}

	public function getAssetsFile() {
		$f = file_get_contents(__DIR__ . "/assetsTemplate.txt");
		$f = $this->doReplacements($f);
		return $f;
	}

	public function getCtrlFile() {
		$f = file_get_contents(__DIR__ . "/mainCtrlTemplate.txt");
		$f = $this->doReplacements($f);
		return $f;
	}

	public function getJSFile() {
		$f = file_get_contents(__DIR__ . "/moduleJSTemplate.txt");
		$f = $this->doReplacements($f);
		return $f;
	}

	public function getProjectFile() {
		$f = file_get_contents(__DIR__ . "/project.txt");
		$f = $this->doReplacements($f);
		return $f;
	}

	public function getSeedsFile() {
		$f = file_get_contents(__DIR__ . "/Seeder.txt");
		$f = $this->doReplacements($f);
		return $f;
	}

	public function setupPackage()
	{
		// mkdir($this->getSrcPath(),0777,true);
		$composer = $this->fixComposer();
		$phpunit = file_get_contents(__DIR__ . '/phpunit.xml');
		$phpunit = str_replace("./vendor/autoload.php","../../vendor/autoload.php",$phpunit);

		file_put_contents(__DIR__ . "/phpunit.xml", $phpunit);
		file_put_contents(__DIR__ . "/composer.json", $composer);
		file_put_contents(__DIR__ . "/README.md", "# {$this->package->description}");
		file_put_contents(
			$this->getSrcPath()."/".$this->getServiceProviderClass().".php", 
			$this->getServiceProviderFile()
		);

		file_put_contents(
			$this->getSrcPath()."/routes.php", 
			$this->getRoutesFile()
		);

		file_put_contents(
			$this->getSrcPath()."/seeds/Seeder.php", 
			$this->getSeedsFile()
		);

		file_put_contents(
			$this->getSrcPath()."/../assets/assets.php", 
			$this->getAssetsFile()
		);

		file_put_contents(
			$this->getSrcPath()."/../assets/scripts/controllers/main.js", 
			$this->getCtrlFile()
		);

		file_put_contents(
			$this->getSrcPath()."/../assets/scripts/module.js", 
			$this->getJSFile()
		);

		file_put_contents(
			$this->getSrcPath()."/../qj-{$this->className}.sublime-project", 
			$this->getProjectFile()
		);

		mkdir($this->getSrcPath()."/../angular/".strtolower($this->package->name),0777,true);

		unlink(__DIR__ . "/tests/CreatePackageTest.php");
		unlink(__DIR__ . "/composer.json.tpl");
		unlink(__DIR__ . "/ServiceProviderTemplate.txt");
		unlink(__DIR__ . "/routesTemplate.txt");
		unlink(__DIR__ . "/assetsTemplate.txt");
		unlink(__DIR__ . "/mainCtrlTemplate.txt");
		unlink(__DIR__ . "/project.txt");
		unlink(__DIR__ . "/Seeder.txt");
		unlink(__DIR__ . "/create-files.php");

		shell_exec('git init');
		return $this;
	}
}

if($argv[1] === 'start') {
	$pkg = new Package;
	$creator = (new CreatePackage($pkg,new Author))->setupPackage();
}

echo "\n";
