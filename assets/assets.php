<?php 
$head = Assets::container('quickjob.head');
$footer = Assets::container('quickjob.footer');

$head
->append(asset('themes/default/main.css'))
->appendStyle(asset('http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700'))
;
$footer->append(asset('themes/default/scripts.js'));
