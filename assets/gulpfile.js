var gulp = require('gulp');
var livereload = require('gulp-livereload');
var mainBowerFiles = require('main-bower-files');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gulpFilter = require('gulp-filter');
var fs = require('fs');
var exec = require('child_process').exec;
var gutil = require('gulp-util');

var scripts = [
	'scripts/module.js',
	'scripts/controllers/**/*.js',
	'scripts/directives/**/*.js',
	'scripts/services/**/*.js',
	'scripts/utils/**/*.js',
];

var needsToPublish = function() {
	fs.writeFile('../needs-to-publish','true');
};

gulp.task('concat',function() {
	needsToPublish();
	return gulp.src(scripts)
	.pipe(concat('scripts.js',{newline:';\n;'}))
	.pipe(gulp.dest('./dist'));
});

gulp.task('compass', function (cb) {
	needsToPublish();
	exec("compass compile", function (error, stdout, stderr) {
		gutil.beep();
		needsToPublish();
		console.log(stdout);
	});
	return gulp.src('styles/*.scss');
});

gulp.task('needs-publish', function (cb) {
	needsToPublish();
});

gulp.task('default',['concat','compass','needs-publish']);

gulp.task('watch',function() {
	livereload.listen();
	gulp.watch('scripts/**/*.js',['concat']);
	gulp.watch('styles/**/*.scss',['compass']);

	gulp.watch('dist/**/*.js').on('change', livereload.changed);
	gulp.watch('dist/**/*.css').on('change', livereload.changed);
});
