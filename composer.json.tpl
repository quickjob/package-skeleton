{
  "name": "quickjob/{{packagename}}",
  "description": "{{description}}",
  "authors": [
    {
      "name": "{{author}}",
      "email": "example@example.com"
    }
  ],
  "require": {
    "php": ">=5.4.0",
    "illuminate/support": "4.1.*",
    "quickjob/loader": "dev-master"
  },
  "require-dev": {
    "phpunit/phpunit": "3.7.*@dev",
    "mockery/mockery": "*@stable"
  },
  "autoload": {
    "psr-4": {
      "Quickjob\\Skeleton\\": "src/"
    },
    "files": [
      "create-files.php"
    ]
  },
  "repositories": [
    {
      "type": "composer",
      "url": "http://repsatis.quickjob.com.br/"
    }
  ],
  "minimum-stability": "dev"
}
